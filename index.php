<?php


trait Hewan {
    public $nama;
    public $darah=50;
    public $jumlahkaki;
    public $keahlian;
  
    public function atraksi($nama,$keahlian) 
    {
      echo "{$this->nama}  sedang {$this->keahlian}";
    }
  }
  
abstract class Fight {
    use Hewan;
    public $attackPower; 
    public $defencePower;

    public function serang($lawan) 
    {
        echo "{$this->nama}  sedang menyerang {$lawan->nama}";
        echo "<br>";
        $lawan->diserang($this);
    }
    
    public function diserang($lawan) 
    {
        echo "{$this->nama}  sedang diserang {$lawan->nama}";

        $this->darah = $this->darah - ($lawan->attackPower / $this->defencePower);
    }

    protected function getInfo()
    {
        echo "Nama          : {$this-> nama}";
        echo "<br>";
        echo "Jumlah Kaki   : {$this-> jumlahkaki}";
        echo "<br>";
        echo "Keahlian      : {$this-> keahlian}";
        echo "<br>";
        echo "Darah         : {$this-> darah}";
        echo "<br>";
        echo "Attack Power  : {$this-> attackPower}";
        echo "<br>";
        echo "Defence Power : {$this-> defencePower}";
    }

    abstract public function getInfoHewan();

}

class Elang extends Fight{
        public function __construct($nama)  
        {
            $this->nama         = $nama;
            $this->jumlahkaki   = 4;                 
            $this->keahlian     = "terbang tinggi";                    
            $this->attackPower  = 10;
            $this->defencePower = 5;
        }

        public function getInfoHewan()
        {
            echo "Jenis Hewan   : Elang";
            echo "<br>";
            $this->getInfo();

        }
    }
      
class Harimau extends Fight{

        public function __construct($nama) 
        {
            $this->nama         = $nama;
            $this->jumlahkaki   = 4;               // $jumlahkaki;
            $this->keahlian     = "berlari cepat"; // $keahlian;
            $this->attackPower  = 7;               // $attackPower;
            $this->defencePower = 8;               // $defencePower;
        }

        public function getInfoHewan()
        {
            echo "Jenis Hewan   : Harimau";
            echo "<br>";
            $this->getInfo();

        }
}    

class Spasi{
    public static function garis()
    {
        echo "<br>";
        echo "=========================";
        echo "<br>";
    }
}

$Harimau = new Harimau("Harimau Sumatera"); 
$Harimau->getInfoHewan();           
Spasi::garis();
$Elang = new Elang("Elang Jawa"); 
$Elang  ->getInfoHewan();
Spasi::garis();

$Harimau->serang($Elang);
Spasi::garis();
$Harimau->getInfoHewan();
Spasi::garis();
$Elang  ->getInfoHewan();
Spasi::garis();

$Elang->serang($Harimau);
Spasi::garis();
$Harimau->getInfoHewan();
Spasi::garis();
$Elang  ->getInfoHewan();
?>

